# Time Tracking App

## Installation

* Install `Docker` & `Docker Compose` on your machine.

* Copy `.env` file from `.env.dist` & insert valid values for variables:
```
cp .env.dist .env
```
* Insert desired values for DB:
```
DATABASE_PASSWORD=foo_bar
DATABASE_NAME=foo_bar
```
* Run the project:
```
docker-compose up -d --build
```
* Install Composer dependencies:
```
docker run --rm --interactive --tty --volume $PWD:/app composer install
```
### JWT key generation

Get-in your container:
```
docker exec -it tta-php-fpm /bin/sh
```
Run commands:
```
openssl genpkey -out config/jwt/private.pem -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096
openssl pkey -in config/jwt/private.pem -out config/jwt/public.pem -pubout
```
You will be asked for pass phrase, so insert it and after don't forget to copy it into `.env` variable (`JWT_PASSPHRASE`).

### Tests

Run:

docker exec tta-php-fpm php ./vendor/bin/simple-phpunit
