<?php declare(strict_types=1);

namespace App\Service\ExportData;

abstract class AbstractExport implements ExportInterface
{
    /**
     * @var array
     */
    protected array $parameters;

    /**
     * @var string
     */
    protected string $filename;

    /**
     * {@inheritDoc}
     */
    public function __construct(array $parameters)
    {
        $this->parameters = $parameters;

        $this->setFilename();
    }

    /**
     * @return string
     */
    protected function createDate(): string
    {
        return date("Y_m_d_h_i_s");
    }

    /**
     * @return void
     */
    protected function setFilename(): void
    {
        if (!isset($this->parameters['path_to_file'])) {
            throw new \InvalidArgumentException("Path in export params hasn't been defined yet");
        }

        $this->filename = sprintf($this->parameters['path_to_file'], $this->createDate());
    }
}
