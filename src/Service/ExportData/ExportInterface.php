<?php declare(strict_types=1);

namespace App\Service\ExportData;

interface ExportInterface
{
    /**
     * @param array $parameters
     */
    public function __construct(array $parameters);

    /**
     * @return string
     */
    public function getFile(): string;

    /**
     * @param array $data
     * @return void
     */
    public function putDataToFile(array $data): void;
}
