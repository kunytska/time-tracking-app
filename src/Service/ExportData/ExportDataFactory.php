<?php declare(strict_types=1);

namespace App\Service\ExportData;

class ExportDataFactory
{
    /**
     * @var string
     */
    private string $format = 'csv';

    /**
     * @var array
     */
    private array $parameters;

    /**
     * @var array
     */
    private array $availableFormat = ['csv'];

    /**
     * @param array $parameters
     * @return ExportInterface
     */
    public function create(array $parameters): ExportInterface
    {
        $this->setParameters($parameters);

        return $this->getObject();
    }

    /**
     * @param array $parameters
     */
    private function setParameters(array $parameters): void
    {
        if (!isset($parameters['format_export'])) {
            $this->setFormat($parameters['format_export']);
        }

        $this->parameters = $parameters;
    }

    /**
     * @param string $format
     * @throws \InvalidArgumentException
     */
    public function setFormat(string $format): void
    {
        if (!in_array($format, $this->availableFormat)) {
            throw new \InvalidArgumentException(sprintf("'%s' format isn't available", $format));
        }

        $this->format = $format;
    }

    /**
     * @return ExportInterface
     * @throws \RuntimeException
     */
    private function getObject(): ExportInterface
    {
        $namespace = __NAMESPACE__ . '\Exporter\\' . $this->getClassName();

        if (!class_exists($namespace)) {
            throw new \RuntimeException(sprintf("'%s' class hasn't been defined yet", $namespace));
        }

        return new $namespace($this->parameters);
    }

    /**
     * @return string
     */
    private function getClassName(): string
    {
        return ucfirst($this->format) . 'Exporter';
    }
}
