<?php declare(strict_types=1);

namespace App\Service\ExportData\Handler;

class ExportFileHandler
{
    private const CONTENT_TYPE_MAP = [
        'csv' => 'text/csv'
    ];

    /**
     * @var string
     */
    private string $content;

    /**
     * @var array
     */
    private array $headers;

    /**
     * @param string $filepath
     * @return void
     */
    public function export(string $filepath): void
    {
        $this->content = file_get_contents($filepath);

        unlink(realpath($filepath));

        $fileInfo = new \SplFileInfo($filepath);

        $this->headers = [
            'Content-Type: '.self::CONTENT_TYPE_MAP[$fileInfo->getExtension()],
            "Content-disposition: attachment;filename={$fileInfo->getFilename()}",
        ];
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }
}
