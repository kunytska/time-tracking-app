<?php declare(strict_types=1);

namespace App\Service\ExportData\Handler;

use App\Entity\User;
use App\Service\ExportData\ExportDataFactory;
use App\Service\ExportData\ExportInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use Symfony\Component\PropertyAccess\PropertyPath;

class ExportDataHandler implements ExportDataHandlerInterface
{
    /**
     * @var array
     */
    protected array $parameters;

    /**
     * @var ExportDataFactory
     */
    protected ExportDataFactory $exporterFactory;

    /**
     * @var ExportInterface
     */
    protected ExportInterface $exporter;

    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $em;

    /**
     * @var PropertyAccessorInterface
     */
    private PropertyAccessorInterface $propertyAccessor;

    /**
     * @var string
     */
    private string $timeFormat = 'Y-m-d H:i:s';

    /**
     * @param array $parameters
     * @param ExportDataFactory $exportDataFactory
     * @param EntityManagerInterface $entityManager
     * @param PropertyAccessorInterface $propertyAccessor
     */
    public function __construct(
        array $parameters,
        ExportDataFactory $exportDataFactory,
        EntityManagerInterface $entityManager,
        PropertyAccessorInterface $propertyAccessor
    ) {
        $this->parameters = $parameters;
        $this->exporterFactory = $exportDataFactory;
        $this->em = $entityManager;
        $this->propertyAccessor = $propertyAccessor;
    }

    /**
     * @param string $name The name of the export
     * @param User $user
     * @return string
     */
    public function handle(string $name, User $user): string
    {
        $this->setParameter($name);
        $this->setExporter();

        /** @var QueryBuilder $qb */
        $qb = $this->em->createQueryBuilder();
        $qb
            ->select('entity')
            ->from($this->parameters['entity'], 'entity');

        $this->handleData($qb);

        return $this->exporter->getFile();
    }

    public function getParameters(): array
    {
        return $this->parameters;
    }

    /**
     * @param string $name
     */
    private function setParameter(string $name)
    {
        if (!isset($this->parameters[$name]) || empty($this->parameters[$name])) {
            throw new \InvalidArgumentException(
                sprintf("Parameters for '%s' haven't been defined yet", $name)
            );
        }

        if (empty($this->parameters[$name]['entity'])) {
            throw new \InvalidArgumentException('Entity parameter for export hasn\'t been defined yet');
        }

        $this->parameters = $this->parameters[$name];
    }

    /**
     * @return void
     */
    private function setExporter(): void
    {
        $this->exporter = $this->exporterFactory->create($this->parameters);
    }

    /**
     * @param QueryBuilder $qb
     */
    private function handleData(QueryBuilder $qb): void
    {
        $result = $qb->getQuery()->execute();

        if (empty($result)) {
            return;
        }

        foreach ($result as $entity) {
            $data = [];
            foreach ($this->parameters['fields'] as $field) {
                $data[] = $this->getValue($this->propertyAccessor->getValue($entity, new PropertyPath($field)));
            }

            $this->exporter->putDataToFile([$data]);
        }
    }

    /**
     * @param $value
     * @return string|null
     */
    private function getValue($value): ?string
    {
        if ($value instanceof \DateTimeInterface) {
            $value = $value->format($this->timeFormat);
        }

        return (string)$value;
    }
}
