<?php declare(strict_types=1);

namespace App\Service\ExportData\Handler;

use App\Entity\User;

interface ExportDataHandlerInterface
{
    /**
     * @param string $name
     * @param User $user
     * @return string
     */
    public function handle(string $name, User $user): string;
}
