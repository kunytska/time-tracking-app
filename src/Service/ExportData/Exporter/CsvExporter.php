<?php declare(strict_types=1);

namespace App\Service\ExportData\Exporter;

use App\Service\ExportData\AbstractExport;

class CsvExporter extends AbstractExport
{
    /**
     * {@inheritDoc}
     */
    public function getFile(): string
    {
        return $this->filename;
    }

    /**
     * {@inheritDoc}
     */
    public function putDataToFile(array $data): void
    {
        $file = $this->getFileObject();

        foreach ($data as $fields) {
            $file->fputcsv($fields);
        }
    }

    /**
     * @return \SplFileObject
     */
    private function getFileObject(): \SplFileObject
    {
        return new \SplFileObject($this->filename, 'a');
    }
}
