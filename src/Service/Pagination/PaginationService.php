<?php declare(strict_types=1);

namespace App\Service\Pagination;

use Doctrine\ORM\Query;
use Doctrine\ORM\Tools\Pagination\Paginator;

class PaginationService
{
    /**
     * @param Query $query
     * @param Pagination $pagination
     * @return PaginatedCollection
     */
    public function paginate(Query $query, Pagination $pagination): PaginatedCollection
    {
        $paginator = new Paginator($query);
        $paginator->getQuery()
            ->setFirstResult($pagination->getOffset())
            ->setMaxResults($pagination->getLimit());

        return new PaginatedCollection(
            (array)$paginator->getIterator(),
            $paginator->count(),
            $pagination->getOffset(),
            $pagination->getLimit()
        );
    }
}
