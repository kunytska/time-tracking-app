<?php declare(strict_types=1);

namespace App\Service\Pagination;

/**
 * @codeCoverageIgnore
 */
class PaginatedCollection
{
    /**
     * @var array
     */
    private array $items;

    /**
     * @var int
     */
    private int $total;

    /**
     * @var int
     */
    private int $count;

    /**
     * @var int
     */
    private int $offset;

    /**
     * @var int
     */
    private int $limit;

    /**
     * @param array $items
     * @param int $total
     * @param int $offset
     * @param int $limit
     */
    public function __construct(array $items, int $total, int $offset, int $limit)
    {
        $this->items = $items;
        $this->total = $total;
        $this->count = count($items);
        $this->offset = $offset;
        $this->limit = $limit;
    }

    /**
     * @return array
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @return int
     */
    public function getOffset(): int
    {
        return $this->offset;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }
}
