<?php declare(strict_types=1);

namespace App\Service\Pagination;

/**
 * @codeCoverageIgnore
 */
class Pagination
{
    public const
        OFFSET_DEFAULT = 0,
        LIMIT_DEFAULT = 15,
        FILTER_DEFAULT = [];

    /** @var int */
    private int $offset;

    /** @var int */
    private int $limit;

    /** @var array */
    private array $filter;

    /**
     * PaginatedRequest constructor.
     *
     * @param int $offset
     * @param int $limit
     * @param array $filter
     */
    public function __construct(
        int $offset = self::OFFSET_DEFAULT,
        int $limit = self::LIMIT_DEFAULT,
        array $filter = self::FILTER_DEFAULT
    ) {
        $this->offset = $offset;
        $this->limit = $limit;
        $this->filter = $filter;
    }

    /**
     * @return int
     */
    public function getOffset(): int
    {
        return $this->offset;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * @return array
     */
    public function getFilter(): array
    {
        return $this->filter;
    }
}
