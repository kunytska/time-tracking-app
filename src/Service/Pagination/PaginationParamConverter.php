<?php declare(strict_types=1);

namespace App\Service\Pagination;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;

class PaginationParamConverter implements ParamConverterInterface
{
    /**
     * @inheritDoc
     */
    public function apply(Request $request, ParamConverter $configuration): bool
    {
        $request->attributes->set(
            $configuration->getName(),
            new Pagination(
                (int)$request->query->get('offset', Pagination::OFFSET_DEFAULT),
                (int)$request->query->get('limit', Pagination::LIMIT_DEFAULT),
                (array)$request->query->get('filter', Pagination::FILTER_DEFAULT)
            )
        );

        return true;
    }

    /**
     * @inheritDoc
     */
    public function supports(ParamConverter $configuration): bool
    {
        return Pagination::class === $configuration->getClass();
    }
}
