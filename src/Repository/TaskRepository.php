<?php declare(strict_types=1);

namespace App\Repository;

use App\Entity\Task;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * @codeCoverageIgnore
 */
class TaskRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Task::class);
    }

    /**
     * @param User $user
     * @param array $filter
     * @return Query
     * @throws ApplicationException
     */
    public function getQuery(User $user, array $filter): Query
    {
        /** @var QueryBuilder $qb */
        $qb = $this->createQueryBuilder('t')
            ->where('t.user = :user')
            ->setParameter('user', $user);

        $classMetaData = $this->getEntityManager()->getClassMetadata(Task::class);

        foreach ($filter as $propertyName => $value) {
            if (!$classMetaData->hasField($propertyName)) {
                throw new BadRequestHttpException(
                    sprintf('Trying to filter by not existing property: %s', $propertyName)
                );
            }

            $qb->andWhere(
                $qb->expr()->eq(
                    sprintf('%s.%s', 't', $propertyName),
                    $qb->expr()->literal($value)
                )
            );
        }

        return $qb
            ->orderBy('t.createdAt', Criteria::DESC)
            ->getQuery();
    }
}
