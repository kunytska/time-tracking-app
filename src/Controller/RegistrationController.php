<?php declare(strict_types=1);

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Serializer\UserNormalizer;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RegistrationController extends AbstractApiController implements JsonControllerInterface
{
    /**
     * @Route("/api/register", methods={"POST"})
     * @param Request $request
     * @param SerializerInterface $serializer
     * @param ValidatorInterface $validator
     * @param UserRepository $userRepository
     * @param UserPasswordEncoderInterface $encoderFactory
     * @return JsonResponse
     */
    public function register(
        Request $request,
        SerializerInterface $serializer,
        ValidatorInterface $validator,
        UserRepository $userRepository,
        UserPasswordEncoderInterface $encoderFactory
    ): JsonResponse {
        /** @var User $user */
        $user = $serializer->deserialize(
            $request->getContent(),
            User::class,
            'json'
        );

        $violations = $validator->validate($user);

        if ($violations->count() > 0) {
            throw new BadRequestHttpException($violations->get(0)->getMessage());
        }

        $user->setPassword(
            $encoderFactory->encodePassword($user, $user->getPassword())
        );
        $userRepository->save($user);

        return $this->createResponse(
            $user,
            Response::HTTP_CREATED,
            '',
            [],
            [UserNormalizer::CONTEXT_KEY => UserNormalizer::CONTEXT_USER]
        );
    }
}
