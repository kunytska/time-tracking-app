<?php declare(strict_types=1);

namespace App\Controller;

use App\Controller\Response\ApiResponse;
use App\Entity\Task;
use App\Entity\User;
use App\Repository\TaskRepository;
use App\Repository\UserRepository;
use App\Service\Pagination\Pagination;
use App\Service\Pagination\PaginationService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class TaskController extends AbstractApiController implements JsonControllerInterface
{
    private User $user;

    /**
     * @param ApiResponse $apiResponse
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(ApiResponse $apiResponse, TokenStorageInterface $tokenStorage)
    {
        parent::__construct($apiResponse);
        /** @var UserInterface|User $user */
        $user = $tokenStorage->getToken()->getUser();
        $this->user = $user;
    }

    /**
     * @Route("/api/tasks", methods={"POST"})
     * @param Request $request
     * @param SerializerInterface $serializer
     * @param ValidatorInterface $validator
     * @param UserRepository $userRepository
     * @return JsonResponse
     */
    public function create(
        Request $request,
        SerializerInterface $serializer,
        ValidatorInterface $validator,
        UserRepository $userRepository
    ): JsonResponse {
        /** @var Task $task */
        $task = $serializer->deserialize(
            $request->getContent(),
            Task::class,
            'json'
        );

        $violations = $validator->validate($task);

        if ($violations->count() > 0) {
            throw new BadRequestHttpException($violations->get(0)->getMessage());
        }

        $this->user->addTask($task);
        $userRepository->save($this->user);

        return $this->createResponse($task, Response::HTTP_CREATED);
    }

    /**
     * @Route("api/tasks", methods={"GET"})
     * @param Pagination $pagination
     * @param PaginationService $paginationService
     * @param TaskRepository $taskRepository
     * @return JsonResponse
     */
    public function list(
        Pagination $pagination,
        PaginationService $paginationService,
        TaskRepository $taskRepository
    ): JsonResponse {
        $query = $taskRepository->getQuery($this->user, $pagination->getFilter());

        $tasks = $paginationService->paginate($query, $pagination);

        return $this->createResponse($tasks);
    }
}
