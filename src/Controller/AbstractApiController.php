<?php declare(strict_types=1);

namespace App\Controller;

use App\Controller\Response\ApiResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * @codeCoverageIgnore
 */
abstract class AbstractApiController extends AbstractController
{
    private ApiResponse $apiResponse;

    /**
     * @param ApiResponse $apiResponse
     */
    public function __construct(ApiResponse $apiResponse)
    {
        $this->apiResponse = $apiResponse;
    }

    /**
     * @param mixed $data
     * @param int $statusCode
     * @param string $message
     * @param array $headers
     * @param array $context
     * @return JsonResponse
     */
    public function createResponse(
        $data,
        int $statusCode = Response::HTTP_OK,
        string $message = '',
        array $headers = [],
        array $context = []
    ): JsonResponse {
        $this->apiResponse->setData($data);
        $this->apiResponse->setMessage($message);

        return $this->json(
            $this->apiResponse,
            $statusCode,
            $headers,
            $context
        );
    }

    /**
     * @param mixed $data
     * @param int $statusCode
     * @param string $message
     * @return JsonResponse
     */
    public function createNotSuccessResponse(
        $data,
        int $statusCode = Response::HTTP_BAD_REQUEST,
        string $message = ''
    ): JsonResponse {
        $this->apiResponse->setSuccess(false);

        return $this->createResponse($data, $statusCode, $message);
    }
}
