<?php declare(strict_types=1);

namespace App\Controller;

use App\Entity\User;
use App\Registry\ExportDataRegistry;
use App\Service\ExportData\Handler\ExportDataHandlerInterface;
use App\Service\ExportData\Handler\ExportFileHandler;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class ExportDataController extends AbstractController implements JsonControllerInterface
{
    /**
     * @var ExportDataRegistry
     */
    private ExportDataRegistry $exportDataRegistry;

    /**
     * @var User
     */
    private User $user;

    /**
     * @param ExportDataRegistry $exportDataRegistry
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(ExportDataRegistry $exportDataRegistry, TokenStorageInterface $tokenStorage)
    {
        $this->exportDataRegistry = $exportDataRegistry;
        /** @var UserInterface|User $user */
        $user = $tokenStorage->getToken()->getUser();
        $this->user = $user;
    }

    /**
     * @Route("/api/export", methods={"POST"})
     * @param Request $request
     * @param ExportFileHandler $exportFileHandler
     * @return Response
     */
    public function export(Request $request, ExportFileHandler $exportFileHandler): Response
    {
        $exportName = $request->get('export');
        /** @var ExportDataHandlerInterface $handler */
        $handler = $this->exportDataRegistry->getExportDataHandler(
            $exportName
        );

        $filename = $handler->handle($exportName, $this->user);

        $exportFileHandler->export($filename);

        return new Response($exportFileHandler->getContent(), 200, $exportFileHandler->getHeaders());
    }
}
