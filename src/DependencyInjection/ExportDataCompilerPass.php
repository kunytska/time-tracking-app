<?php declare(strict_types=1);

namespace App\DependencyInjection;

use App\Registry\ExportDataRegistry;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class ExportDataCompilerPass implements CompilerPassInterface
{
    private const TAG = 'app.handler.export_data_handler';

    /**
     * @inheritDoc
     */
    public function process(ContainerBuilder $container): void
    {
        if (!$container->hasDefinition(ExportDataRegistry::class)) {
            return;
        }

        $definition = $container->findDefinition(ExportDataRegistry::class);

        foreach ($container->findTaggedServiceIds(self::TAG) as $id => $tags) {
            foreach ($tags as $tag) {
                if (!isset($tag['alias'])) {
                    throw new \RuntimeException(
                        sprintf(
                            'Please set alias for tag "%s" in service "%s"',
                            static::TAG,
                            $id
                        )
                    );
                }

                $definition->addMethodCall('addExportDataHandler', [
                    $tag['alias'],
                    new Reference($id)
                ]);
            }
        }
    }
}
