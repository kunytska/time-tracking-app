<?php declare(strict_types=1);

namespace App\EventSubscriber;

use App\Controller\JsonControllerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\Exception\UnsupportedMediaTypeHttpException;
use Symfony\Component\HttpKernel\KernelEvents;

class JsonRequestSubscriber implements EventSubscriberInterface
{
    /**
     * @param ControllerEvent $event
     */
    public function onKernelController(ControllerEvent $event): void
    {
        $controller = $event->getController();

        /**
         * $controller passed can be either a class or a Closure.
         * This is not usual in Symfony but it may happen.
         * If it is a class, it comes in array format
         */
        if (!is_array($controller)) {
            return;
        }

        if ($controller[0] instanceof JsonControllerInterface) {
            $request = $event->getRequest();

            if ($request->headers->get('Content-Type') === 'application/json') {
                $data = json_decode($request->getContent());

                foreach ((array)$data as $key => $value) {
                    $request->request->set($key, $value);
                }

                return;
            }

            throw new UnsupportedMediaTypeHttpException("Content-Type value should be 'application/json'");
        }
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::CONTROLLER => 'onKernelController'
        ];
    }
}
