<?php declare(strict_types=1);

namespace App\Registry;

use App\Service\ExportData\Handler\ExportDataHandlerInterface;

class ExportDataRegistry
{
    private string $defaultHandlerName = "default";

    /**
     * @var array
     */
    private array $handlers = [];

    /**
     * @param string $name
     * @param $exportDataHandler
     */
    public function addExportDataHandler(string $name, ExportDataHandlerInterface $exportDataHandler): void
    {
        $this->handlers[$name] = $exportDataHandler;
    }

    /**
     * @param string $name
     * @return ExportDataHandlerInterface
     */
    public function getExportDataHandler(string $name): ExportDataHandlerInterface
    {
        if (!isset($this->handlers[$name]) && !isset($this->handlers[$this->defaultHandlerName])) {
            throw new \RuntimeException(sprintf('Export handler "%s" is not register', $name));
        }

        if (isset($this->handlers[$name])) {
            return $this->handlers[$name];
        }

        return $this->handlers[$this->defaultHandlerName];
    }
}
