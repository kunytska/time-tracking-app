<?php declare(strict_types=1);

namespace App\EventListener;

use App\Controller\Response\ApiResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class ExceptionListener
{
    private NormalizerInterface $normalizer;

    private ApiResponse $apiResponse;

    /**
     * @param NormalizerInterface $normalizer
     * @param ApiResponse $apiResponse
     */
    public function __construct(NormalizerInterface $normalizer, ApiResponse $apiResponse)
    {
        $this->normalizer = $normalizer;
        $this->apiResponse = $apiResponse;
    }

    /**
     * @param ExceptionEvent $event
     * @throws ExceptionInterface
     */
    public function onKernelException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();

        $response = new JsonResponse;

        if ($exception instanceof HttpExceptionInterface) {
            $response->setStatusCode($exception->getStatusCode());
            $response->headers->replace($exception->getHeaders());
            $this->apiResponse->setMessage($exception->getMessage());
        } else {
            $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $event->setResponse(
            $response->setData($this->getResponseData($exception))
        );
    }

    /**
     * @param \Throwable $exception
     * @return array
     * @throws ExceptionInterface
     */
    private function getResponseData(\Throwable $exception): array
    {
        $this->apiResponse->setSuccess(false);

        // could be used in 'debug' mode
        $this->apiResponse->setException($exception);

        return $this->normalizer->normalize(
            $this->apiResponse
        );
    }
}
