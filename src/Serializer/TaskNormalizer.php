<?php declare(strict_types=1);

namespace App\Serializer;

use App\Entity\Task;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class TaskNormalizer implements NormalizerInterface
{
    private const IGNORED_ATTRIBUTES = ['user'];

    /**
     * @var NormalizerInterface
     */
    private NormalizerInterface $normalizer;

    /**
     * @param ObjectNormalizer $normalizer
     */
    public function __construct(ObjectNormalizer $normalizer)
    {
        $this->normalizer = $normalizer;
    }

    /**
     * @inheritDoc
     */
    public function normalize($object, $format = null, array $context = [])
    {
        return $this->normalizer->normalize(
            $object,
            $format,
            $context + [AbstractNormalizer::IGNORED_ATTRIBUTES => self::IGNORED_ATTRIBUTES]
        );
    }

    /**
     * @inheritDoc
     */
    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof Task;
    }
}
