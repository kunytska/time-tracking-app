<?php declare(strict_types=1);

namespace App\Serializer;

use App\Entity\User;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class UserNormalizer implements NormalizerInterface
{
    public const CONTEXT_KEY = 'user_context';

    public const CONTEXT_USER = 'user';

    public const CONTEXT_ATTRIBUTES = [
        self::CONTEXT_USER => [
            AbstractNormalizer::IGNORED_ATTRIBUTES => [
                'password', 'salt', 'roles', 'username',
            ],
        ],
    ];

    /**
     * @var NormalizerInterface
     */
    private NormalizerInterface $normalizer;

    /**
     * @param ObjectNormalizer $normalizer
     */
    public function __construct(ObjectNormalizer $normalizer)
    {
        $this->normalizer = $normalizer;
    }

    /**
     * @inheritDoc
     */
    public function normalize($object, $format = null, array $context = [])
    {
        return $this->normalizer->normalize($object, $format, $context + $this->getContext($context));
    }

    /**
     * @param $context
     *
     * @return array
     * @codeCoverageIgnore
     */
    private function getContext(array $context): array
    {
        if (!empty($context[self::CONTEXT_KEY])) {
            $context += self::CONTEXT_ATTRIBUTES[$context[self::CONTEXT_KEY]] ?? [];
        }

        return $context;
    }

    /**
     * @inheritDoc
     */
    public function supportsNormalization($data, $format = null): bool
    {
        return $data instanceof User;
    }
}
